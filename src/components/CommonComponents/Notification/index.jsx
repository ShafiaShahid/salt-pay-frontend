import notification from './Notification';

const Notification = (type, message, description) => {
    notification[type]({
        message,
        description,
    });
};
export default Notification;
