import React from 'react';
import { Select } from 'antd';

const { Option } = Select;

const SearchBar = (props) => {

  return (
    <Select
      showSearch
      style={{ width: '85%' }}
      placeholder="Search products"
      optionFilterProp="children"
      onChange={props.onChange}
      onSearch={props.onSearch}
      filterOption={(input, option) =>
        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
      onSelect={props.onSelect}
    >
      {props.result && props.result.map(opt => <Option value={opt.product_name}>{opt.product_name}</Option>)}
    </Select>
  )
}


export default SearchBar