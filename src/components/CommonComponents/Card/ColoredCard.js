import React, { useState, useEffect } from 'react';
import cardIcon from '../../../assets/images/cardIcon.svg'
import './card.less';
const ColoredCard = (props) => {



    return (
        <div className='isoStickerWidget'>
            <div className='isoIconWrapper'>
                <img src={cardIcon}/>
            </div>
            <div className='isoContentWrapper'>
                <span className='isoLabel'>{props.data}</span>
                <h3 className='isoStatNumber'>{props.header}</h3>

            </div>
        </div>
    )
}


export default ColoredCard;