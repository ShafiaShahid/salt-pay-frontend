import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';
import { Card, Button } from 'antd';
import ProductPlaceholder from '../../assets/images/placeholderProduct.png';
import './cartScreen.less'

const CartScreen = (props) => {
    const product = props.location.state.product;
    const qty = props.location.state.qty;
    const { cartItems } = props;
    useEffect(() => {
        const { addToCart } = props;
        if (props.location.state.product.length > 0) {
            addToCart(product, qty)

        }
        props.history.push({ pathname: '/cart', state: { product: [], qty: [] } })

    }, [])

    const handleRemove = (item) => {
        const { removeFromCart } = props;
        removeFromCart(item)
    }
    return (

        <div>
            {cartItems && cartItems.length > 0 ? cartItems.map(item => (
                <Card>
                    <div className='cart-main'>
                        <div className='cart-img'>
                            <img alt='alt' src={item.image && item.image.length > 0 ? item.image : ProductPlaceholder} className='cart-imgstyle' />

                        </div>
                        <div className='cart-det'>
                            <h1>{item.name}</h1>
                            <p><b>Qty: </b>{item.qty}</p>

                        </div>
                        <Button className='btn-primary mr-10' onClick={(eitem) => handleRemove(item)}>Remove Item</Button>
                    </div>
                </Card>
            )) : <Card><p>Cart is empty</p></Card>}
        </div>
    )
}


const mapStateToProps = state => {
    return {
        cartItems: state.cartReducer.cartItems,

    }
}
const mapDispatchToProps = dispatch => {
    return {
        addToCart: (product, qty) => dispatch(actions.addToCart(product, qty)),
        removeFromCart: (product) => dispatch(actions.removeFromCart(product)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CartScreen));