import React, { useState } from 'react';
import { Card, Button, Select } from 'antd';
import { withRouter } from 'react-router-dom';
import ProductPlaceholder from '../../assets/images/placeholderProduct.png';
import './productDetail.less';

const { Option } = Select;

const ProductDetail = (props) => {
    const [quantity, setQuantity] = useState(1);
    const { product } = props.location.state;

    const handleCart = (e) => {
        props.history.push({
            pathname: '/cart',
            state: {
                product: product,
                qty: quantity
            }
        })
    }

    const handleChange = (value) => {
        setQuantity(value)
        console.log(`selected ${value}`);
    }
    return (
        <div className="site-card-wrapper">
            <Card className='proddetail-card'>
                <div className='proddetail-maindiv'>
                    <div className='proddetail-imgdiv'>
                        <img alt='alt' src={product.image_url && product.image_url.length > 0 ? product.image_url : ProductPlaceholder} className='proddetail-imgstyle' />

                    </div>
                    <div className='proddetail-detdiv'>
                        <h1>{product.product_name}</h1>
                        {product.generic_name.length > 0 && <p><b>Common Name: </b>{product.generic_name}</p>}

                        {product.quantity.length > 0 && <p><b>Quantity: </b>{product.quantity}</p>}
                        {product.brands.length > 0 && <p><b>Brands: </b>{product.brands}</p>}
                        {product.stores.length > 0 && <p><b>Stores: </b>{product.stores}</p>}
                        {product.origins.length > 0 && <p><b>Origins: </b>{product.origins}</p>}




                        {product.countries.length > 0 && <p><b>Countries: </b>{product.countries}</p>}

                        {product.ingredients_text.length > 0 && <p><b>Ingredient text: </b>{product.ingredients_text}</p>}
                        {product.energy_100g.length > 0 && <p><b>Energy 100g: </b>{product.energy_100g}</p>}
                        {product.proteins_100g.length > 0 && <p><b>Proteins 100g: </b>{product.proteins_100g}</p>}



                        <div>
                            <span>Qty:  </span>
                            <Select defaultValue="1" style={{ width: 80 }} onChange={handleChange}>
                                <Option value="1">1</Option>
                                <Option value="2">2</Option>
                                <Option value="3">3</Option>
                            </Select>
                        </div>

                        <Button className='btn-primary mr-10' onClick={(e) => handleCart(e)}>Add to Cart</Button>
                    </div>
                </div>

            </Card>
        </div>
    )
}




export default withRouter(ProductDetail);