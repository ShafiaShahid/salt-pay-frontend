import React, { useState } from 'react';
import { Card, Col, Row, Radio, Spin } from 'antd';
import { useSelector } from "react-redux";
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import './allProductScreen.less';

const Filters = (props) => {
    const [value, setValue] = useState('');
    const loading = useSelector(state => state.productReducer.getAllCountriesLoading)

    const onChange = (e) => {
        setValue(e.target.value)
        const data = {
            search: e.target.value,
            pageSize: 0,
            pageLimit: 12
        }
        props.getAllProductsPagination(data, res => {
            if (!res) {
            } else {

            }
        })
    }


    return (
        <div>
            <Col span={4} className='filter-row'>
                {loading ?

                    <div className='spin-div'>
                        <Spin />
                    </div>
                    :
                    <>
                        <h1>Filter By</h1>
                        <Radio.Group onChange={onChange} value={value}>
                            {props.countries && props.countries.tags && props.countries.tags.map(((country) =>
                                <div>
                                    <Radio value={country.name}>{country.name}</Radio>
                                </div>
                            ))}
                        </Radio.Group>
                    </>
                }
            </Col>
        </div>
    )
}


const mapStateToProps = state => {
    return {
        getAllProductsLoading: state.productReducer.getAllProductsLoading,

    }
}
const mapDispatchToProps = dispatch => {
    return {
        getAllProductsPagination: (data, res) => dispatch(actions.getAllProductsPagination(data, res)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Filters);

