import React, { useEffect, useState } from 'react';
import { Pagination, Col, Row, Card } from 'antd';
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';
import Product from '../Product/Product';
import Filters from './Filters';
import './allProductScreen.less';

const AllProductScreen = (props) => {
    const [products, setProducts] = useState([]);
    const [totalCount, setTotalCount] = useState(0);
    const [pageSize, setPageSize] = useState(1);
    const [pageLimit, setPageLimit] = useState(12);
    const [countries, setCountries] = useState([]);



    //will run as the page renders
    useEffect(() => {
        const { getAllProducts, getAllCountries } = props;
        getAllProducts(res => {
            if (!res.status) {

            } else {
                setProducts(res.data.data)
                setTotalCount(res.data.totalDataCount)
            }
        })

        getAllCountries(res => {
            if (!res.status) {

            } else {
                setCountries(res.data)
            }
        })
    }, []);

    const handlePagination = (page, pagelimit) => {
        const { getAllProductsPagination } = props;
        setPageSize(page);
        setPageLimit(pagelimit);
        const data = {
            pageSize: page,
            pageLimit:pagelimit
        }

        getAllProductsPagination(data , res => {
            if (!res.status) {

            } else {
                setProducts(res.data.data)
                setTotalCount(res.data.totalDataCount)
            }
        })


 
    }

    return (
        <div className="layout">
            <div className='sider'>
                <Filters countries={countries}/>
            </div>
            <div className='content'>
                <Row gutter={2, 4} >
                    {props.getAllProductsSuccess && props.getAllProductsSuccess.data.map(product =>
                        <Col span={{ xs: 12, sm: 8, md: 5, lg: 4 }} className='allproducts-row'>
                            <Product product={product} />
                        </Col>)}
                </Row>
                <div className='pagination'>
                    <Card className='pagination-card'>
                    <Pagination
                        total={totalCount}
                        showTotal={totalCount => `Total ${totalCount} items`}
                        pageSizeOptions={[12, 18, 24, 36]}
                        defaultPageSize={12}
                        defaultCurrent={1}
                        onChange={handlePagination}
                    />
                    </Card>
                </div>
            </div>

        </div>
    )
}


const mapStateToProps = state => {
    return {
        getAllProductsLoading: state.productReducer.getAllProductsLoading,
        getAllProductsSuccess: state.productReducer.getAllProductsSuccess
        
    }
}
const mapDispatchToProps = dispatch => {
    return {
        getAllProducts: (res) => dispatch(actions.getAllProducts(res)),
        getAllProductsPagination: (data, res) => dispatch(actions.getAllProductsPagination(data, res)),
        getAllCountries: (res) => dispatch(actions.getAllCountries(res)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AllProductScreen);