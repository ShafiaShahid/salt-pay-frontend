import React, { Fragment } from 'react';
import { Card } from 'antd';
import ProductPlaceholder from '../../assets/images/placeholderProduct.png';
import './product.less';
import { Link } from 'react-router-dom';
const Product = (props) => {
    const { product } = props;
    return (
        <Fragment>
            <Card className='product-cardstyle'>
                <div className='info-card'>
                    <img alt='alt' src={product.image_url && product.image_url.length > 0 ? product.image_url : ProductPlaceholder} className='product-imgstyle' />
                    <Link className='product-productname'
                        to={{
                            pathname: "/product-detail",
                            search: `?productName=${product.product_name}`,
                            state: { product: product }
                        }}>{product.product_name}</Link>
                </div>
            </Card>
        </Fragment>
    )
}

export default Product;