import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import {
    ShoppingCartOutlined,
    HomeOutlined
} from '@ant-design/icons';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import SearchBar from '../CommonComponents/SearchBar/SearchBar';
import * as actions from '../../store/actions/index';
import './topbar.less';
const { Header } = Layout;

class TopBar extends Component {
    state = {
        current: '',
        value: '',
        result: ''
    }

    componentWillMount() {
        if (this.props.location.pathname.includes('cart')) {
            this.setState({ current: 'cart' })

        } else if (this.props.location.pathname && this.props.location.pathname === '/') {
            this.setState({ current: 'products' })

        }
    }

    componentWillReceiveProps(nextProp) {
        if (nextProp.location.pathname.includes('cart')) {
            this.setState({ current: 'cart' })


        } else if (nextProp.location.pathname && nextProp.location.pathname === '/') {
            this.setState({ current: 'products', })

        }

    }

    onChange = (value) => {
        this.setState({ value })
    }


    onSearch = (val) => {
        const data = {
            search: val,
            pageSize: 0,
            pageLimit: 12
        }
        this.props.getAllProductsPagination(data, res => {
            if (!res) {

            } else {
                this.setState({ result: res.data.data })

            }
        })
    }

    onSelect = (val) => {
        const data = {
            search: val,
            pageSize: 0,
            pageLimit: 12
        }
        this.props.getAllProductsPagination(data, res => {
            if (!res) {

            } else {
                this.setState({ result: res.data.data })

            }
        })
    }

    render() {
        const { current, result } = this.state;

        return (
            <Header>
                <div className="logo">
                    <div className='log-itle'>
                        <Link className='title' to='/'>Products</Link>
                        <SearchBar onChange={this.onChange} onSearch={this.onSearch} result={result} onSelect={this.onSelect} />
                    </div>
                    <Menu theme="dark" mode="horizontal" defaultSelectedKeys={[current.toString()]} selectedKeys={[current.toString()]} >
                        <Menu.Item key="products"><Link to='/'><HomeOutlined className='icon' /></Link></Menu.Item>
                        <Menu.Item key="cart"><Link to='/cart'><ShoppingCartOutlined className='icon' /></Link></Menu.Item>
                    </Menu>

                </div>
            </Header>
        )
    }

}



const mapStateToProps = state => {
    return {
        getAllProductsLoading: state.productReducer.getAllProductsLoading,

    }
}
const mapDispatchToProps = dispatch => {
    return {
        getAllProductsPagination: (data, res) => dispatch(actions.getAllProductsPagination(data, res)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TopBar));
