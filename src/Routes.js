import React, { useState, useEffect } from 'react';
import { Redirect, Route, Switch, BrowserRouter as Router, withRouter } from 'react-router-dom';
import App from './App';


const Routes = () => {
//differentiate route on the basis of auth using conditional statement
    let routes = (
        <Switch>
            <Route path="/" component={App} />
        </Switch>
    )
//no auth required route it directly to landing

    return (
        <Router>
            {routes}
        </Router>
    )
}

export default withRouter(Routes)