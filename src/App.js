import React from 'react';
import { Layout, Menu, Breadcrumb } from 'antd';
import TopBar from './components/TopBar/TopBar';
import { withRouter, Route, Switch } from 'react-router-dom';
import AllProductScreen from './components/AllProduct/AllProductScreen';
import CartScreen from './components/Cart/CartScreen';
import './App.less';
import ProductDetail from './components/ProductDetail/ProductDetail';
const { Content, Footer } = Layout;

function App() {
  return (
    <Layout className="App-header">
      <TopBar />
      <Content className="site-layout-background"
        style={{
          padding: '10px 0px',
          margin: 0,
          minHeight: 280,
         
        }}
      >
        <Switch>
          <Route exact path='/' render={({ match }) => <AllProductScreen match={match} />} />
          <Route path='/cart' render={({ match }) => <CartScreen match={match} />} />
          <Route path='/product-detail' render={({ match }) => <ProductDetail match={match} />} />
        </Switch>

      </Content>
      <Footer style={{ textAlign: 'center' }}>SaltPay ©2020</Footer>
    </Layout>
  );
}

export default withRouter(App);
