import * as actionTypes from './actionTypes';
import Cookie from "js-cookie";

export const addToCart = (product, qty) => (dispatch, getState) => {
    try {
      const {cartItems} = getState().cartReducer;
      dispatch({
        type: actionTypes.CART_ADD_ITEM, payload: {
          product: product._id,
          name: product.product_name,
          image: product.image_url,
          qty
        }
      });
     
      localStorage.setItem("cartItems", JSON.stringify(cartItems))
    } catch (error) {
  
    }
  }


  export const removeFromCart = (product) => (dispatch, getState) => {
    dispatch({ type: actionTypes.CART_REMOVE_ITEM, payload: product });
  
    const {cartItems} = getState().cartReducer;
    if(cartItems.length === 0){
    localStorage.removeItem("cartItems")

    } else{
      localStorage.setItem("cartItems", JSON.stringify(cartItems))

    }


  }