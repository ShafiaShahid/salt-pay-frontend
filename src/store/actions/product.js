import * as actionTypes from './actionTypes';
import axios from 'axios';
import config from '../../config';

//get all product listing action

export const getAllProductsFailure = (error) => {
    return {
        type: actionTypes.GET_ALL_PRODUCTS_FAILURE,
        error: error
    }
}

export const getAllProductsSuccess = (data) => {
    return {
        type: actionTypes.GET_ALL_PRODUCTS_SUCCESS,
        data: data
    }
}

export const getAllProductsLoading = (data) => {
    return {
        type: actionTypes.GET_ALL_PRODUCTS_LOADING,
        data: data
    };
}

//get product listing without pagination
export const getAllProducts = (callback) => {
    return (dispatch) => {
        dispatch(getAllProductsLoading(true));
        return axios.get(`${config.baseApiUrl}/products`)
            .then((response) => {
                dispatch(getAllProductsSuccess(response.data));
                dispatch(getAllProductsLoading(true));
                callback({data: response.data, status: true});
                dispatch(getAllProductsLoading(false));
            })
            .catch((err) => {
                dispatch(getAllProductsLoading(false));
                dispatch(getAllProductsFailure(err));
                callback({message: err.msg, status: false});

            })
    }
}

//get product listing with pagination and page limits
export const getAllProductsPagination = (data ,callback) => {
    return (dispatch) => {
        dispatch(getAllProductsLoading(true));
        let url = `${config.baseApiUrl}/products?page=${data.pageSize}&limit=${data.pageLimit}`
        if(data.country){
            url = `${config.baseApiUrl}/products?page=${data.pageSize}&limit=${data.pageLimit}&country=${data.country}`
        } 
        if (data.search){
            url = `${config.baseApiUrl}/products?page=${data.pageSize}&limit=${data.pageLimit}&search=${data.search}`

        } if (data.country && data.search){
            url = `${config.baseApiUrl}/products?page=${data.pageSize}&limit=${data.pageLimit}&search=${data.search}&country=${data.country}`

        }
        return axios.get(url)
            .then((response) => {
                dispatch(getAllProductsSuccess(response.data));
                dispatch(getAllProductsLoading(true));
                callback({data: response.data, status: true});
                dispatch(getAllProductsLoading(false));
            })
            .catch((err) => {
                dispatch(getAllProductsLoading(false));
                dispatch(getAllProductsFailure(err));
                callback({message: err.msg, status: false});

            })
    }
}





//get all countries action


export const getAllCountriesFailure = (error) => {
    return {
        type: actionTypes.GET_ALL_COUNTRIES_FAILURE,
        error: error
    }
}

export const getAllCountriesSuccess = (data) => {
    return {
        type: actionTypes.GET_ALL_COUNTRIES_SUCCESS,
        data: data
    }
}

export const getAllCountriesLoading = (data) => {
    return {
        type: actionTypes.GET_ALL_COUNTRIES_LOADING,
        data: data
    };
}

//get all countries
export const getAllCountries = (callback) => {
    return (dispatch) => {
        dispatch(getAllCountriesLoading(true));
        return axios.get(`${config.baseApiUrl}/countries`)
            .then((response) => {
                dispatch(getAllCountriesSuccess(response.data));
                dispatch(getAllCountriesLoading(true));
                callback({data: response.data, status: true});
                dispatch(getAllCountriesLoading(false));
            })
            .catch((err) => {
                dispatch(getAllCountriesLoading(false));
                dispatch(getAllCountriesFailure(err));
                callback({message: err.msg, status: false});

            })
    }
}
