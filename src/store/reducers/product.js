import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    //get all products initial states
    getAllProductsLoading: false,
    getAllProductsSuccess: null,
    getAllProductsFailure: null,

    //get all Countries initial states
    getAllCountriesLoading: false,
    getAllCountriesSuccess: null,
    getAllCountriesFailure: null,
}

//all products reducer functions
const getAllProductsFailure = (state, action) => {
    return updateObject(state, { getAllProductsFailure: action.data, getAllProductsLoading: false });

}
const getAllProductsSuccess = (state, action) => {
    return updateObject(state, { getAllProductsSuccess: action.data, getAllProductsFailure: false });

}

const getAllProductsLoading = (state, action) => {
    return updateObject(state, { getAllProductsLoading: action.data, getAllProductsFailure: null, });

}


//all Countries reducer functions
const getAllCountriesFailure = (state, action) => {
    return updateObject(state, { getAllCountriesFailure: action.data, getAllCountriesLoading: false });

}
const getAllCountriesSuccess = (state, action) => {
    return updateObject(state, { getAllCountriesSuccess: action.data, getAllCountriesFailure: false });

}

const getAllCountriesLoading = (state, action) => {
    return updateObject(state, { getAllCountriesLoading: action.data, getAllCountriesFailure: null, getAllCountriesSuccess: false });

}

const reducer = (state = initialState, action) => {

    switch (action.type) {

        //all products conditionals
        case actionTypes.GET_ALL_PRODUCTS_FAILURE:
            return getAllProductsFailure(state, action)
        case actionTypes.GET_ALL_PRODUCTS_SUCCESS:
            return getAllProductsSuccess(state, action)
        case actionTypes.GET_ALL_PRODUCTS_LOADING:
            return getAllProductsLoading(state, action)

        //all Countries conditionals
        case actionTypes.GET_ALL_COUNTRIES_FAILURE:
            return getAllCountriesFailure(state, action)
        case actionTypes.GET_ALL_COUNTRIES_SUCCESS:
            return getAllCountriesSuccess(state, action)
        case actionTypes.GET_ALL_COUNTRIES_LOADING:
            return getAllCountriesLoading(state, action)
        default:
            return state;
    }

}

export default reducer;

