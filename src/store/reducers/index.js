import  { combineReducers} from 'redux';
import productReducer from './product';
import cartReducer from './cart';

const rootReducer = combineReducers({
    productReducer,
    cartReducer
})

export default rootReducer