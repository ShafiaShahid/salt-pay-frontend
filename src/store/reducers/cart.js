import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
  cartItems: []
}

const addCartItem = (state, action) => {
  const item = action.payload;
  const product = state.cartItems.find(x => {
    return x.product === item.product
  });
  if (product) {
    return {
      cartItems:
        state.cartItems.map(x => x.product === product.product ? item : x)
    };
  }
  return {
    cartItems: [...state.cartItems, item]
  };

}
const removeCartItem = (state, action) => {
  console.log(state.cartItems)
  const item = state.cartItems.filter(x => {
    return x.product !== action.payload.product
  })
  return updateObject(state, { cartItems: item });


}

const reducer = (state = initialState, action) => {

  switch (action.type) {

    case actionTypes.CART_ADD_ITEM:
      return addCartItem(state, action)
    case actionTypes.CART_REMOVE_ITEM:
      return removeCartItem(state, action)
    default:
      return state;
  }

}

export default reducer;

